# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://klogar@bitbucket.org/klogar/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/klogar/stroboskop/commits/2e78dccbba5bcd6312279e3aebebd4155a560de7

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/klogar/stroboskop/commits/39dff86427f74a95bc6173b738c8c99486a31ef8

Naloga 6.3.2:
https://bitbucket.org/klogar/stroboskop/commits/67da3f6206f47cc1fad47010fd5f9dae737e233a

Naloga 6.3.3:
https://bitbucket.org/klogar/stroboskop/commits/d7e959dacf85910f2b8bd0cf46e4c26a5e741a14

Naloga 6.3.4:
https://bitbucket.org/klogar/stroboskop/commits/1a25d9c6e7f23c8f3125265d7899866075b0f124

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/klogar/stroboskop/commits/6600d954ca84bc8fdb61ee36fe1531390ff2a986

Naloga 6.4.2:
https://bitbucket.org/klogar/stroboskop/commits/e63c1c7d50fb65104a9663882609a04f8c784af9

Naloga 6.4.3:
https://bitbucket.org/klogar/stroboskop/commits/77fc0c2f3c32457b411f4015882b99d3eae0a264

Naloga 6.4.4:
https://bitbucket.org/klogar/stroboskop/commits/4fbe07aa12e3a5c7f151fd40a5cef47dfc041ac4